extends Area2D
class_name Square

signal moveToHere(location)

func _on_Square_area_entered(area: Area2D) -> void:
	var movePath: MovementPath = area as MovementPath
	if movePath != null:
		movePath.connect("ValidMovePath", self, "makeSquareLegalMove")

func _on_Square_area_exited(area: Area2D) -> void:
	var movePath: MovementPath = area as MovementPath
	if movePath != null:
		if movePath.is_connected("ValidMovePath", self, "makeSquareLegalMove"):
			movePath.disconnect("ValidMovePath", self, "makeSquareLegalMove")
	resetSquare()


func makeSquareLegalMove():
	$MoveLegalityOverlay.set_frame_color(Color(0.34, 1, 0.67, 0.35))
	if !is_connected("input_event", self, "emitMoveToHere"):
		connect("input_event", self, "emitMoveToHere")

func resetSquare():
	$MoveLegalityOverlay.set_frame_color(Color(0.34, 1, 0.67, 0))
	if is_connected("input_event", self, "emitMoveToHere"):
		disconnect("input_event", self, "emitMoveToHere")

func emitMoveToHere(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		emit_signal("moveToHere", global_position)


func _on_Square_body_entered(body: Node) -> void:
	print("bodyEntered")
	if !is_connected("moveToHere", body, "taken"):
		print("connected to %s" % body)
		connect("moveToHere", body, "taken")


func _on_Square_body_exited(body: Node) -> void:
	print("BodyExited")
	if is_connected("moveToHere", body, "taken"):
		print("disconnecting %s" % body)
		disconnect("moveToHere", body, "taken")
