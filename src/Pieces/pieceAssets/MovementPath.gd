extends Area2D
class_name MovementPath

onready var nextPath: MovementPath = getNextPath()

signal ValidMovePath
signal EnableEnPassant

var OverLappingSquare: Square
var OverLappingPiece: Piece
var OverLappingMovepaths: Array

func getNextPath() -> MovementPath:
	for child in get_children():
		var x = child as MovementPath
		if x != null:
			return child
	return null


func _on_MovementPath_area_shape_entered(area_id: int, area: Area2D, area_shape: int, self_shape: int) -> void:
	var square = area as Square
	if square != null:
		OverLappingSquare = area
		return
	var x = area as MovementPath
	if x != null:
		OverLappingMovepaths.append(area)


func _on_MovementPath_area_shape_exited(area_id: int, area: Area2D, area_shape: int, self_shape: int) -> void:
	var square = area as Square
	if square != null:
		OverLappingSquare = null	
	var x = area as MovementPath
	if x != null:
		var y = OverLappingMovepaths.find(x)
		if y != -1:
			OverLappingMovepaths.remove(y)


func _on_MovementPath_body_entered(body: Node) -> void:
	if body is Piece:
		OverLappingPiece = body


func _on_MovementPath_body_exited(body: Node) -> void:
	if body is Piece:
		OverLappingPiece = null
