extends KinematicBody2D
class_name Piece, "res://assets/chessPieces/whiteQueen.png"

enum pieceColor {WHITE, BLACK}

signal hasMoved

onready var sprite: Sprite = $Sprite
onready var MovementPaths: Array = $MovementPaths.get_children()
var pinnedToKing: = false
export(pieceColor) var color

func _ready():
	set_process(false)

func _process(delta: float) -> void:
	sprite.set_global_position(get_viewport().get_mouse_position())

func _on_Piece_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		calculateFullMovement(MovementPaths)

func calculateFullMovement(movePaths: Array) -> void:
	get_tree().call_group_flags(2, "Pieces", "Unselect")
	set_process(true)
	get_tree().call_group_flags(2, "Squares", "resetSquare")
	
	for path in movePaths:
		calculateFullPath(path)

func calculateFullPath(destination: MovementPath) -> void:
	var endLoop:= false
	if destination.nextPath == null:
		endLoop = true
		
	if destination.OverLappingSquare != null:
		if destination.OverLappingPiece == null:
			validPath(destination, destination.OverLappingSquare)
			if !endLoop:
				calculateFullPath(destination.nextPath)
			return
		if destination.OverLappingPiece.color != color:
			validPath(destination, destination.OverLappingSquare)
		
func Unselect():
	sprite.set_position(Vector2(0,0))
	set_process(false)
	
	var signalList: Array = get_incoming_connections()
	for sig in signalList:
		if sig.get("signal_name") == "moveToHere" and sig.get("method_name") == "move":
			print(sig)
			sig.get("source").disconnect(sig.get("signal_name"), self, sig.get("method_name"))
		

func validPath(path: MovementPath, square: Square):
	path.emit_signal("ValidMovePath")
	square.connect("moveToHere", self, "move")
	
	
func move(destination: Vector2):
	set_position(destination)
	Unselect()
	
func taken(_x):
	print("TAKEN")
	queue_free()
