extends Piece
class_name King

export(NodePath) var RookRight
export(NodePath) var RookLeft

var rookRight: Rook
var rookLeft: Rook


func _ready() -> void:
	._ready()
	rookRight = $RookRight
	rookLeft = $RookLeft
	

func _on_Piece_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	._on_Piece_input_event(viewport, event, shape_idx)
	

func calculateFullMovement(movePaths: Array) -> void:
	.calculateFullMovement(movePaths)
	

func calculateFullPath(destination: MovementPath) -> void:
	.calculateFullPath(destination)
