extends Piece
class_name Pawn

onready var EnPassantLeft: MovementPath = $SpecialtyMovement/EnPassantLeft
onready var EnPassantRight: MovementPath = $SpecialtyMovement/EnPassantRight
onready var CaptureLeft: MovementPath = $SpecialtyMovement/CaptureLeft
onready var CaptureRight: MovementPath = $SpecialtyMovement/CaptureRight
onready var Forward: MovementPath = $SpecialtyMovement/Forward
var hasMoved: = false
var justMovedTwice: = false

enum EnPassantStatus {NEITHER, LEFT, RIGHT}

var enableEnPassant = EnPassantStatus.NEITHER

func _ready() -> void:
	._ready()

func _on_Piece_input_event(viewport: Node, event: InputEvent, shape_idx: int):
	._on_Piece_input_event(viewport, event, shape_idx)

func calculateFullMovement(movePaths: Array):
	.calculateFullMovement(movePaths)
	calculateCapture(CaptureLeft)
	calculateCapture(CaptureRight)
	calculateForward(Forward)
	
	if enableEnPassant == EnPassantStatus.RIGHT:
		calculateEnPassant(CaptureRight, EnPassantRight)
	if enableEnPassant == EnPassantStatus.LEFT:
		calculateEnPassant(CaptureLeft, EnPassantLeft)
	

func calculateEnPassant(destination: MovementPath, checkTile: MovementPath):
	if destination.OverLappingSquare != null and destination.OverLappingPiece == null:
		validPath(destination, destination.OverLappingSquare)

func calculateCapture(destination: MovementPath):
	if destination.OverLappingSquare != null and destination.OverLappingPiece != null and destination.OverLappingPiece.color != color:
		validPath(destination, destination.OverLappingSquare)

func calculateForward(destination: MovementPath):
	var endloop: = false
	if destination.nextPath == null:
		endloop = true
	
	if destination.OverLappingSquare != null and destination.OverLappingPiece == null:
		validPath(destination, destination.OverLappingSquare)
		if !endloop:
			calculateForward(destination.nextPath)


func _on_EnPassantLeft_body_entered(body: Node) -> void:
	if checkEnPassantPossible(body):
		enableEnPassant = self.LEFT


func _on_EnPassantRight_body_entered(body: Node) -> void:
	if checkEnPassantPossible(body):
		enableEnPassant = self.RIGHT

func checkEnPassantPossible(body):
	var pawn: Pawn = body as Pawn
	if pawn != null and pawn.justMovedTwice:
		return true
	else: 
		return false

func move(destination: Vector2):
	.move(destination)
	hasMoved = true

func unselect():
	.Unselect()
